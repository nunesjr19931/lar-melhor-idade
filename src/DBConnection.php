<?php

namespace LarMelhorIdade;

class DBConnection {

    private $_dbHostname = "127.0.0.1";
    private $_dbName = "larIdosos";
    private $_dbUsername = "root";
    private $_dbPassword = "tarefa2_pass";
    private $_con;

    public function __construct() {
        try {
            $this->_con = new \PDO("mysql:host=$this->_dbHostname;dbname=$this->_dbName", $this->_dbUsername, $this->_dbPassword);
            $this->_con->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            echo "Conectado com sucesso.";
        } catch(\PDOException $e) {
            echo "<h1>Ops, algo deu errado: " . $e->getMessage()."</h1>";
            echo "<pre>";
            echo print_r($e);
        }

    }

    public function getConnection() {
        return $this->_con;
    }
}
?>