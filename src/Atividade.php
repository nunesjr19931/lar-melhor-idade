<?php

namespace LarMelhorIdade;

class Atividade
{
    /** @var string */
    private $nome;
    /** @var AtividadeRepository */
    private $repository;


    public function __construct(AtividadeRepository $repository)
    {

        $this->repository = $repository;
    }

    public function save($nome): bool
    {
        if($this->validateActivityAlreadyExists($nome)){
            throw new \LogicException("There is already an activity called $nome.");
        }
        return $this->repository->save($nome);

    }

    /**
     * @param string $nome
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param Atividade $atividade
     */
    private function validateActivityAlreadyExists($nomeAtividade): bool
    {
        return $this->repository->checkIfRecordAlreadyExists($nomeAtividade);
    }

}