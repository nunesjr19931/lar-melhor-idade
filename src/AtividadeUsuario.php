<?php

namespace LarMelhorIdade;

use http\Exception\InvalidArgumentException;
use PHPUnit\Util\Exception;

class AtividadeUsuario
{

    private $repository;
    /**
     * @var Paciente
     */
    private $paciente;
    /**
     * @var Atividade
     */
    private $atividade;

    public function __construct(AtividadeUsuarioRepository $repository, Paciente $paciente, Atividade $atividade)
    {

        $this->repository = $repository;
        $this->paciente = $paciente;
        $this->atividade = $atividade;
    }

    public function register(\DateTime $data)
    {
        $now = new \DateTime('now');
        $diffDatetimes = $now->diff($data);

        if ($diffDatetimes->format('%R') === '+' && $diffDatetimes->format('%a') > 0) {
            throw new \InvalidArgumentException('It is not allowed to register an activity for a future date.');
        }

        if ($this->validateActivity($this->paciente, $this->atividade, $data)) {
            throw new \InvalidArgumentException(\sprintf(
                'There is already a log of activity %s for user %s on day %s.',
                $this->atividade->getNome(),
                $this->paciente->getNome(),
                $data->format('Y/m/d')
            ));
        }

        return $this->repository->save($this->paciente, $this->atividade, $data);
    }

    private function validateActivity(Paciente $paciente, Atividade $atividade, \DateTime $data): bool
    {
        return $this->repository->checkIfRecordAlreadyExists($paciente, $atividade, $data);
    }


}