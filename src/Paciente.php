<?php

namespace LarMelhorIdade;

class Paciente
{
    /** @var string */
    private $nome;

    /** @var string */
    private $cpf;

    /** @var string */
    private $id;

    private $repository;

    /**
     * @var PacienteRepository
     */

    public function __construct(PacienteRepository $repository)
    {

        $this->repository = $repository;
    }

    public function create()
    {
        return $this->repository->save();
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

}