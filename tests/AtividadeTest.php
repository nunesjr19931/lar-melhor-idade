<?php

use LarMelhorIdade\Atividade;
use LarMelhorIdade\AtividadeRepository;
use PHPUnit\Framework\TestCase;

class AtividadeTest extends TestCase
{
    /** @var AtividadeRepository|\PHPUnit\Framework\MockObject\MockObject */
    private $stubRepository;

    /** @var Atividade */
    private $atividadeStub;
    /**
     * @var AtividadeRepository|mixed|\PHPUnit\Framework\MockObject\MockObject
     */
    private $repositoryStub;

    public function setUp(): void
    {
        $this->repositoryStub = $this->createMock(AtividadeRepository::class);
        $this->atividadeStub = new Atividade($this->repositoryStub);
        parent::setUp();
    }

    /**
     * Testando Cenário 3: Registro de atividade
     */
    public function testCreateActivityShouldReturnSuccess()
    {
        $this->repositoryStub->method('checkIfRecordAlreadyExists')->with(
            $this->anything()
        )->willReturn(false);
        $this->repositoryStub->method('save')->with($this->anything())->willReturn(true);
        $this->assertTrue($this->atividadeStub->save('Caminhada'));
    }

    /**
     * Testando Cenário 4: Cadastro de atividade repetida para um idoso
     */
    public function testCreateActivityAlreadyRegisteredShouldReturnError()
    {
        $this->repositoryStub->method('checkIfRecordAlreadyExists')->with(
            $this->anything()
        )->willReturn(true);

        $this->expectException(\LogicException::class);
        $this->expectErrorMessage('There is already an activity called Corrida.');
        $this->atividadeStub->save('Corrida');
    }


}